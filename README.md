# RECAST for Z LFV with tau (hadronic tau channels) analysis

## Analysis info

Glance: https://glance.cern.ch/atlas/analysis/analyses/details.php?id=1132

Support note: https://cds.cern.ch/record/2684086

Analysis group contact: [atlas-phys-exotics-lfv-z-with-tau-full-run2@cern.ch](mailto:atlas-phys-exotics-lfv-z-with-tau-full-run2@cern.ch)


## Analysis workflow

Analysis code repository: https://gitlab.cern.ch/ATLAS-Z-LFV/Z-LFV-full-run2

This workflow of the analysis consists of the following steps:
1. Ntuple production from DAOD (recast step: `ntuple_production`)
1. Merge ntuples into a single file and compute MC normalisation weights (recast step: `merge_ntuples`)
1. Split ntuples into smaller trees with region-specific selections (recast step: `split_ntuples`)
1. Decorate ntuples with neural network classifier outputs (recast step: `decorate_NN_outputs`)
1. Decorate ntuples with Z pT correction weights (weights=1 for non Z LFV samples)(recast step: `decorate_ZpT_weights`)
1. Perform binned likelihood fit and extract discovery significance / exclusion limit (recast step: `fit`)

Required inputs (accessible by xrdcp):
- DAODs of your signal samples
  - in all three MC campaigns: MC16a/r9364, MC16d/r10201, MC16e/r10724
  - release: AthDerivation 21.2.77 / AMI-Tag: p3978
  - example: `/eos/project/r/recast/atlas/ANA-EXOT-2018-36/signal_DAOD/*`
- Pileup reweighting files with pileup profiles of the signal samples
  - separated in MC campaigns: one file for each campaign
  - example: `/eos/project/r/recast/atlas/ANA-EXOT-2018-36/PRW_files/*`
- Xsec file with cross section (and k-factor and/or filter efficiency) of the signal samples
  - example: `/eos/project/r/recast/atlas/ANA-EXOT-2018-36/xsec_files/susy_xsec_13TeV.txt`

Parameters:
- `CHANNEL`: `el` for running the e+tau_had channel, `mu` for running the mu+tau_had channel
- `SIGNAL_NAME`: `Ztaue` for running the e+tau_had channel, `Ztaumu` for running the mu+tau_had channel (other user-chosen names are currently not supported)
- `SIGNAL_DSID`: DSID of the signal
- `SIGNAL_DAOD_16a`/`SIGNAL_DAOD_16d`/`SIGNAL_DAOD_16e`: Path to the MC16a/MC16d/MC16e signal DAODs (accessible by xrdcp)
- `PRW_FILE_16a`/`PRW_FILE_16d`/`PRW_FILE_16e`: Path to the MC16a/MC16d/MC16e pileup reweighting file (accessible by xrdcp) / `DEFAULT` for using the default file
- `XSEC_FILE`: Path to the MAC16e pileup reweighting file (accessible by xrdcp) / `DEFAULT` for using the default file
- `MAX_EVENTS`: Number of events to process per DAOD file / `0` for processing all
- `FIT_ARGS`: Optional arguments for the likelihood fitting (ref. [fit arguments](https://gitlab.cern.ch/ATLAS-Z-LFV/Z-LFV-full-run2/-/blob/a101e629326723a4c52cda98ed320dbe209ffb5b/analysis/fit.py#L25))


## Setup and commands

Central RECAST documentation: https://recast-docs.web.cern.ch/recast-docs/

On a local machine, do:
```
pip install recast-atlas
```

On lxplus, do:
```
source ~recast/public/setup.sh
```

Example command (run in the root directory of this repository):
```
export RECAST_USER=<user_id>
export RECAST_PASS=<user_password>
export RECAST_TOKEN=<user_GitLab_token>
eval "$(recast auth setup -a $RECAST_USER -a $RECAST_PASS -a $RECAST_TOKEN -a default)"
eval "$(recast auth write --basedir authdir)"
$(recast catalogue add $PWD)
recast catalogue ls
recast catalogue describe ZLFV_tauhad
recast catalogue check ZLFV_tauhad
recast run ZLFV_tauhad --tag run
```
Note: Currently the workflow fails to run on lxplus because of the read-only constraints set by Singularity and atlas-recast.
This can hopefully be solved in the future with an atlas-recast update.
It has been tested that individual steps work when the `--writable` option for Singularity is used.

